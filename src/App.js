import './App.css';
import AuthorizationForm from './AuthorizationForm';

function App() {
  return (
    <div className = "App">
    <div className ="auth_column">
        <AuthorizationForm></AuthorizationForm>
    </div>
    </div>
  );
}

export default App;
